const express = require('express');
const { addDog, getDogs, ping } = require('./controllers/dog')
const connectToDatabase = require('./dbConntection')

const app = express();

const poolPromise = connectToDatabase();

poolPromise.catch((err) => {
  console.error('Connection error:', err);
  process.exit(1);
});

app.get('/ping', ping);

app.get('/dog', getDogs);

app.post('/dog', addDog);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
