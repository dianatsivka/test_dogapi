const { DataTypes } = require('sequelize');

const model = (sequelize) => {
    const attributes = {
        name: { type: DataTypes.STRING, allowNull: false },
        color: { type: DataTypes.STRING, allowNull: false },
        tail_length: { type: DataTypes.NUMBER, allowNull: false },
        weight: { type: DataTypes.NUMBER, allowNull: false }
    };

    return sequelize.define('Dog', attributes);
}

module.exports = model;
