const { Dog } = require('../dbConnection');

const addDog = async (req, res) => {
  const { name, color, tail_length, weight } = req.body;

  if (typeof tail_length !== 'number' || tail_length < 0) {
    return res.status(400).json({ error: 'Invalid tail length' });
  }

  try {
    const existingDog = await Dog.findOne({ where: { name } });

    if (existingDog) {
      return res.status(409).json({ error: 'Dog with specified name already exists' });
    }

    const newDog = await Dog.create({ name, color, tail_length, weight });

    res.status(201).json(newDog);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

const getDogs = async(req, res) => {
  const { attribute, order, limit = 10, ...filters } = req.query;

  try {
    const orderCriteria = [];
    if (attribute && order) {
      const validAttributes = ['name', 'color', 'tail_length', 'weight'];
      if (validAttributes.includes(attribute) && (order === 'asc' || order === 'desc')) {
        orderCriteria.push([attribute, order.toUpperCase()]);
      }
    }

    const options = {
      where: filters,
      order: orderCriteria,
      limit: limit,
    };

    const dogs = await Dog.findAll(options);

    res.json(dogs);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

const ping = (req, res) => {
  res.send('Dogshouseservice.Version1.0.1');
}

module.exports = {
  addDog, 
  getDogs,
  ping
}