const tedious = require('tedious');
const { Sequelize } = require('sequelize');

const { dbName, dbConfig } = require('config.json');

const initialize = async() => {
    const dialect = 'mssql';
    const host = dbConfig.server;
    const { userName, password } = dbConfig.authentication.options;

    await ensureDbExists(dbName);

    const sequelize = new Sequelize(dbName, userName, password, { host, dialect });

    db.Dog = require('../models/dog.model')(sequelize);

    await sequelize.sync({ alter: true });
}

const ensureDbExists = (dbName) => {
    return new Promise((resolve, reject) => {
        const connection = new tedious.Connection(dbConfig);
        connection.connect((err) => {
            if (err) {
                console.error(err);
                reject(`Connection Failed: ${err.message}`);
            }

            const createDbQuery = `IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = '${dbName}') CREATE DATABASE [${dbName}];`;
            const request = new tedious.Request(createDbQuery, (err) => {
                if (err) {
                    console.error(err);
                    reject(`Create DB Query Failed: ${err.message}`);
                }

                resolve();
            });

            connection.execSql(request);
        });
    });
}

initialize();

module.exports = db = {};
